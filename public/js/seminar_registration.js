$("#btn_register").on('click', function(e){
	var url = $(this).data('url');
	var group_id = $("#group_id").val();
	var seminar_id = $("#seminar_id").val();
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
	    	type:"POST",
	        url:url,
	        data:{group_id:group_id,seminar_id:seminar_id},
	        cache:false,
	        beforeSend:function(){
	           $("#btn_register").prop('disabled', true);
	        },
	    	success:function(response){
	         // console.log(response);
	        if(response.status == true){
		       window.location = response.redirect;
	        }else{
	        	if(response.type == 'duplicate'){
	        		location.reload();
	        	}else if(response.type == 'capacity_full'){
	        		window.location = response.redirect;
	        	}
		        $("#btn_register").prop('disabled', false);
	        }
	    },
	    error:function(error){
	        console.log(error);
	        $("#btn_register").prop('disabled', false);
	    }
	});
});

$("#btn_cancel").on('click', function(e){
	var url = $(this).data('url');
	var seminar_registration_id = $("#seminar_registration_id").val();
	var seminar_id = $("#seminar_id").val();
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
	    	type:"POST",
	        url:url,
	        data:{seminar_registration_id:seminar_registration_id,seminar_id:seminar_id},
	        cache:false,
	        beforeSend:function(){
	           $("#btn_cancel").prop('disabled', true);
	        },
	    	success:function(response){
	         // console.log(response);
	        if(response.status == true){
		       window.location = response.redirect;
	        }else{
		        $("#btn_cancel").prop('disabled', false);
	        }
	    },
	    error:function(error){
	        console.log(error);
	        $("#btn_cancel").prop('disabled', false);
	    }
	});
});