@extends('Layout.app_auth')

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center mt-3">
    <div class="col-sm-8">
          <form action="{{ route('auth.add')}}" id="registration_form">

     <div class="card">
       <div class="card-header">Registration Form</div>
       <div class="card-body">
         <div class="row">
          <input type="hidden" id="id" name="id" placeholder="" class="form-control" required>
          <div class="position-relative mb-2 col-sm-6">
            <label>First Name </label>
            <input type="text" id="first_name" name="first_name" placeholder="Enter Firstname here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_first_name"></div>
          </div>
          <div class="position-relative mb-2 col-sm-6">
            <label>Middle Name </label>
            <input type="text" id="middle_name" name="middle_name" placeholder="Enter Middlename here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_middle_name"></div>
          </div>
          <div class="position-relative mb-2 col-sm-12">
            <label>Last Name </label>
            <input type="text" id="last_name" name="last_name" placeholder="Enter Lastname here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_last_name"></div>
          </div>
          <div class="position-relative mb-2 col-sm-6">
            <label>Gender </label>
            <select id="gender" name="gender" class="form-control form-select">
              <option value="Male ">Male </option>
              <option value=" Female "> Female </option>
              <option value=" Others"> Others</option>
            </select>
            <div class="invalid-feedback" id="err_gender"></div>
          </div>
            <div class="position-relative mb-2 col-sm-6">
            <label>Age </label>
            <input type="number" id="Age" name="Age" placeholder="Enter Age here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_Age"></div>
          </div>
          <div class="position-relative mb-2 col-sm-12">
            <label>Interested in </label>
            <select id="Interested_in" name="Interested_in" class="form-control form-select">
              <option value="Male ">Male </option>
              <option value=" Female "> Female </option>
              <option value=" Other"> Other</option>
            </select>
            <div class="invalid-feedback" id="err_Interested in"></div>
          </div>
            <div class="position-relative mb-2 col-sm-12">
            <label>Address </label>
            <input type="text" id="address" name="address" placeholder="Enter Full Address here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_address"></div>
          </div>
          <div class="position-relative mb-2 col-sm-12">
            <label>Email Address </label>
            <input type="email" id="email_address" name="email_address" placeholder="Enter Email here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_email_address"></div>
          </div>
          <div class="position-relative mb-2 col-sm-12">
            <label>Password </label>
            <input type="password" id="password" name="password" placeholder="Enter Password here" class="form-control form-control" required>
            <div class="invalid-feedback" id="err_password"></div>
          </div>
         
       </div>
     </div>
       <div class="card-footer text-right">
         <button type="button" class=" px-4 btn btn-danger">Clear</button>
         <button type="submit" class=" px-4 btn btn-success">Save</button>
       </div>
    </div>
       </form>

  </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	 $("#registration_form").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_submit_users').prop('disabled', true);
          $('#btn_submit_users').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          console.log(response)
          swal("Success", response.message, "success");
          showValidator(response.error,'registration_form');
          show_users();
          $('#modal_users').modal('hide');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'registration_form');
        }
        $('#btn_submit_users').prop('disabled', false);
        $('#btn_submit_users').text('Save');
      },
      error:function(error){
        console.log(error)
      }
    });
  });
</script>
@endsection