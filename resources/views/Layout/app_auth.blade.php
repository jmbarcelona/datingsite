<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>

	<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
			@include('Layout.auth_navBar')
			
			@yield('content')
	</body>
	@include('Layout.footer')
	@yield('script')
</html>