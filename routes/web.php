<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserAccountController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FindLoverController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



	Route::get('/', [AuthController::class, 'login'])->name('auth.login');
	Route::post('/login', [AuthController::class, 'checkLogin'])->name('auth.checkLogin');
	Route::get('/registration', [AuthController::class, 'registration'])->name('auth.register');
	Route::post('/add', [AuthController::class, 'add'])->name('auth.add');

	Route::get('/logout', [AuthController::class, 'logoutUser'])->name('auth.logout');



Route::group(['middleware' => ['auth', 'AuthAdmin']], function(){
	Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');


});




// User account modules
Route::group(['middleware' => ['auth', 'AuthUser']], function(){
	Route::get('/account', [UserAccountController::class, 'index'])->name('account.index');

	Route::group(['prefix' => 'find-lover', 'as' => 'find-lover.'], function(){
	Route::get('', [FindLoverController::class, 'index'])->name('index');
	
});

});