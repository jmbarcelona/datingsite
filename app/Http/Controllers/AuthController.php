<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    public function login(){
    	return view('Auth.index');
    }

    public function registration(){
    	return view('Auth.registration');
    }

    public function checkLogin(Request $request){
    	$email_address = $request->get('email_address');
    	$password = $request->get('password');
    	$validator = Validator::make($request->all(), [
			'email_address' => 'required|email',
			'password' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			$redirect_url = '';
			$credentials = ['email_address' => $request->get('email_address'), 'password' => $request->get('password')];
			if (Auth::attempt($credentials)) {
				 /*If username and password is correct it will start and set the session or also known as Auth*/
				 $request->session()->regenerate();
				 $user = Auth::user(); /*$_SESSION['auth']*/
				 if ($user->user_type === 1) {
				 	$redirect_url = route('dashboard.index');
				 }else{
				 	$redirect_url = route('account.index');
				 }
				 return response()->json(['status' => true, 'redirect' => $redirect_url]);
			}else{
				$validator->errors()->add('password','Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);	
			}
		}
    }

    public function logoutUser(){
		Session::flush();
    	Auth::logout();
		return redirect(route('auth.login'));
	}

	public function add(Request $request){
		$id= $request->get('id');
		$first_name = $request->get('first_name');
		$middle_name = $request->get('middle_name');
		$last_name = $request->get('last_name');
		$gender = $request->get('gender');
		$Age = $request->get('Age');
		$Interested_in = $request->get('Interested_in');
		$address = $request->get('address');
		$email_address = $request->get('email_address');
		$password = $request->get('password');
		$verify_password = $request->get('verify_password');

		$validator = Validator::make($request->all(), [
			'first_name' => 'required',
			'middle_name' => 'required',
			'last_name' => 'required',
			'gender' => 'required',
			'Age' => 'required',
			'Interested_in' => 'required',
			'address' => 'required',
			'email_address' => 'required|email',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($id)) {
				$users = user::find($id);
				$users->first_name = $first_name;
				$users->middle_name = $middle_name;
				$users->last_name = $last_name;
				$users->gender = $gender;
				$users->Age = $Age;
				$users->Interested_in = $Interested_in;
				$users->address = $address;
				$users->email_address = $email_address;
				$users->password = $password;
				if($users->save()){
					return response()->json(['status' => true, 'message' => 'Users updated successfully!']);
				}
			}else{
				$users = new user;
				$users->first_name = $first_name;
				$users->middle_name = $middle_name;
				$users->last_name = $last_name;
				$users->gender = $gender;
				$users->Age = $Age;
				$users->Interested_in = $Interested_in;
				$users->address = $address;
				$users->email_address = $email_address;
				$users->password = $password;
				if($users->save()){
					return response()->json(['status' => true, 'message' => 'User saved successfully!']);
				}
			}
		}
	}
}
